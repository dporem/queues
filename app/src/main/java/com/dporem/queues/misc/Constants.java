package com.dporem.queues.misc;

public class Constants {
	// Shared Preferences repository name
    public static final String SHARED_PREFERENCES = "com.dporem.queues.SHARED_PREFERENCES";
    // Key in the repository for the previous activity
    public static final String KEY_LOG_LEVEL = "com.dporem.queues.KEY_LOG_LEVEL";
    public static final int DEFAULT_LOG_LEVEL = Log.LOGCAT;
    public static final String LOG_FILENAME = "Queues.log";

	public final static String PARSE_APPLICATIONID = "44MNFEehMtif8vDZaHu4ZdhD1kTq8M7iFhTsX6ws";
	public final static String PARSE_CLIENTKEY = "Tpgy8ps8yZxhqU6sXK7M8QEXLMkeBQOqur2tweay";
	
	public final static String BR_LOGIN_SUCCESSFUL = "BR_LOGIN_SUCCESSFUL";
	public final static String BR_LOGIN_UNSUCCESSFUL = "BR_LOGIN_UNSUCCESSFUL";
	public final static String BR_UPDATE_DB_FINISHED = "BR_UPDATE_DB_FINISHED";
	public final static String BR_PARTICIPANT_INSERT_SUCCESSFUL = "BR_PARTICIPANT_INSERT_SUCCESSFUL";
	public final static String BR_PARTICIPANT_INSERT_UNSUCCESSFUL = "BR_PARTICIPANT_INSERT_UNSUCCESSFUL";
	public final static String BR_PARTICIPANT_INSERT_ALREADY_EXISTS = "BR_PARTICIPANT_INSERT_ALREADY_EXISTS";
	public final static String BR_PARTICIPANT_LIST_UPDATE = "BR_PARTICIPANT_LIST_UPDATE";
	public final static String BR_QUEUE_INSERT_SUCCESSFUL = "BR_QUEUE_INSERT_SUCCESSFUL";
	public final static String BR_QUEUE_INSERT_UNSUCCESSFUL = "BR_QUEUE_INSERT_UNSUCCESSFUL";
	public final static String BR_QUEUE_INSERT_ALREADY_EXISTS = "BR_QUEUE_INSERT_ALREADY_EXISTS";
	public final static String BR_QUEUE_LIST_UPDATE = "BR_QUEUE_LIST_UPDATE";
	public final static String BR_QUEUE_PARTICIPANT_INSERT_SUCCESSFUL = "BR_QUEUE_PARTICIPANT_INSERT_SUCCESSFUL";
	public final static String BR_QUEUE_PARTICIPANT_INSERT_UNSUCCESSFUL = "BR_QUEUE_PARTICIPANT_INSERT_UNSUCCESSFUL";
	public final static String BR_QUEUE_PARTICIPANT_INSERT_ALREADY_EXISTS = "BR_QUEUE_PARTICIPANT_INSERT_ALREADY_EXISTS";
	public final static String BR_ADVANCE_QUEUE_SUCCESS = "BR_ADVANCE_QUEUE_SUCCESS";
	
	public final static String BUNDLE_CODE_PARSECLASS_USERS = "BUNDLE_CODE_PARSECLASS_USERS";
	public final static String BUNDLE_CODE_ADVANCE_QUEUE_ACTIVITY = "com.dporem.queues.BUNDLE_CODE_ADVANCE_QUEUE_ACTIVITY";
	
	public final static int MENU_ID_PARTICIPANT = 0;
	public final static int MENU_ID_INSERT_PARTICIPANT = 1;
	public final static int MENU_ID_INSERT_QUEUE = 2;
	
	public final static String PARSE_CLASSNAME_USER = "Users";
	public final static String PARSE_CLASSNAME_PARTICIPANT = "Participants";
	public final static String PARSE_CLASSNAME_QUEUE = "Queue";
	public final static String PARSE_CLASSNAME_QP_RELATE = "QP_Relate";
}
