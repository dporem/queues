package com.dporem.queues.misc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Log {
	private static final String TAG = Log.class.getCanonicalName();
			
	public static final int NONE = 0;
	public static final int LOGCAT = 1;
	public static final int FILE = 2;
	
	private static final int BUILDER_CAPACITY = 1024;
	private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSZ";
	
	private static Log log;	
	private static int logLevel;
	
	private SharedPreferences mPrefs;	
	private static Context context;
	private static File logFile;
	private static StringBuilder builder = null;
	
	private Log(Context context){
		if(context != null){
			Log.context = context;
			
			mPrefs = context.getSharedPreferences(
	                Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
			logLevel = mPrefs.getInt(Constants.KEY_LOG_LEVEL,
	                Constants.DEFAULT_LOG_LEVEL);						
		}
	}
	
	public void setLogLevel(Context context, int newLogLevel){
		logLevel = newLogLevel;
		
		Editor editor = mPrefs.edit();
        editor.putInt(Constants.KEY_LOG_LEVEL, newLogLevel);
        editor.commit();
	}
	
	/**
     * Returns a new file object 
     */
    private static File createLogFile() {

        // Create a new file in the app's directory
        File newFile = new File(context.getFilesDir(), Constants.LOG_FILENAME);

        // Log the file name
        Log.i(TAG, "Creating new Log File");

        // return the new file handle
        return newFile;
    }
    
    /**
     * Delete log files
     */
    public boolean removeLogFiles() {
        // Start with the "all files removed" flag set to true
        boolean removed = true;

        // Iterate through all the files in the app's file directory
        for (File file : context.getFilesDir().listFiles()) {

            // If unable to delete the file
            if (!file.delete()) {

                // Log the file's name
                Log.e(TAG, file.getAbsolutePath() + " : " + file.getName());

                // Note that not all files were removed
                removed = false;
            }
        }

        // Return true if all files were removed, otherwise false
        return removed;
    }
    private static void writeToLog(String level, String tag, String message){
    	writeToLog(level, tag, message, null);
    }
    
    private static void writeToLog(String level, String tag, String message, Throwable throwable){
    	if(builder==null){
    		builder = new StringBuilder(BUILDER_CAPACITY);
    	}
    	
    	SimpleDateFormat mDateFormat = (SimpleDateFormat) DateFormat.getDateTimeInstance();
    	mDateFormat.applyPattern(DATE_FORMAT_PATTERN);
        mDateFormat.applyLocalizedPattern(mDateFormat.toLocalizedPattern());
        
    	builder.append(mDateFormat.format(new Date())).append(" - ")
    						.append(level).append(" - ")
    						.append(tag).append(" - ")
    						.append(message);
    	if(throwable!=null)
    		builder.append(tag).append(" - ").append(throwable.getMessage());
    	builder.append("\n");
    	
    	if(builder.length()>=(BUILDER_CAPACITY-(BUILDER_CAPACITY/4))){
    		/*if(logFile == null || !logFile.exists()){
        		logFile = createLogFile();
        	}*/	
    		
    		OutputStreamWriter outputStreamWriter;
			try {
				outputStreamWriter = new OutputStreamWriter(context.openFileOutput(Constants.LOG_FILENAME, Context.MODE_APPEND));
				outputStreamWriter.write(builder.toString());
	            outputStreamWriter.close();
	            builder = null;
			} catch (FileNotFoundException e) {
				e(TAG, "Error Writing to file - File not found", e);
			} catch (IOException e) {
				e(TAG, "Error Writing to file - IOException", e);
			}            
    	}
    }
    
    
	public static Log getInstance(Context context) {
		if(log == null){
			log = new Log(context); 
		}
		return log;
	}

	public static void i(String tag, String message){
		if(logLevel != NONE){			
			writeToLog("[Info]", tag, message);
			if(logLevel!=FILE){
				android.util.Log.i(tag, message);
			}				
		}		
	}
	
	public static void i(String tag, String message, Throwable throwable){
		if(logLevel != NONE){			
			writeToLog("[Info]", tag, message, throwable);
			if(logLevel!=FILE){
				android.util.Log.i(tag, message, throwable);
			}
		}
		
	}
	
	public static void d(String tag, String message){
		if(logLevel != NONE){			
			writeToLog("[Debug]", tag, message);
			if(logLevel!=FILE){
				android.util.Log.d(tag, message);
			}			
		}
		
	}
	
	public static void d(String tag, String message, Throwable throwable){
		if(logLevel != NONE){			
			writeToLog("[Debug]", tag, message, throwable);
			if(logLevel!=FILE){
				android.util.Log.d(tag, message, throwable);
			}
		}
		
	}
	
	public static void e(String tag, String message){
		if(logLevel != NONE){			
			writeToLog("[Error]", tag, message);
			if(logLevel!=FILE){
				android.util.Log.e(tag, message);
			}
		}
		
	}
	
	public static void e(String tag, String message, Throwable throwable){
		if(logLevel != NONE){			
			writeToLog("[Error]", tag, message, throwable);
			if(logLevel!=FILE){
				android.util.Log.e(tag, message, throwable);
			}
		}
		
	}
	
	public static void v(String tag, String message){
		if(logLevel != NONE){			
			writeToLog("[Verbose]", tag, message);
			if(logLevel!=FILE){
				android.util.Log.v(tag, message);
			}
		}
		
	}
	
	public static void v(String tag, String message, Throwable throwable){
		if(logLevel != NONE){			
			writeToLog("[Verbose]", tag, message, throwable);
			if(logLevel!=FILE){
				android.util.Log.v(tag, message, throwable);
			}
		}
		
	}
	
	public static void w(String tag, String message){
		if(logLevel != NONE){			
			writeToLog("[Warning]", tag, message);
			if(logLevel!=FILE){
				android.util.Log.w(tag, message);
			}
		}
		
	}
	
	public static void w(String tag, String message, Throwable throwable){
		if(logLevel != NONE){			
			writeToLog("[Warning]", tag, message, throwable);
			if(logLevel!=FILE){
				android.util.Log.w(tag, message, throwable);
			}
		}
		
	}
	
	public static void wtf(String tag, String message){
		if(logLevel != NONE){			
			writeToLog("[What a Terrible Failure]", tag, message);
			if(logLevel!=FILE){
				android.util.Log.wtf(tag, message);
			}
		}
		
	}
	
	public static void wtf(String tag, String message, Throwable throwable){
		if(logLevel != NONE){			
			writeToLog("[What a Terrible Failure]", tag, message, throwable);
			if(logLevel!=FILE){
				android.util.Log.wtf(tag, message, throwable);
			}
		}		
	}
}
