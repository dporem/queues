package com.dporem.queues.misc;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.QPRelate;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.parseclasses.Users;

public class DatabaseHelper extends SQLiteOpenHelper {	

	private static final String TAGLOG = DatabaseHelper.class.getSimpleName();
	 
	private static DatabaseHelper instance = null;	
	
    // Database Version
    private static final int DATABASE_VERSION = 3;
 
    // Database Name
    private static final String DATABASE_NAME = "queuesManager";
 
    // Table Names
    public static final String TABLE_USER = "user";
    public static final String TABLE_QUEUES = "queues";
    public static final String TABLE_PARTICIPANTS = "participants";
    public static final String TABLE_QUEUES_PARTICIPANTS = "queues_participants";
 
    // Common column names
    private static final String KEY_ID = "_id";    
    private static final String KEY_OBJECTID = "objectId";
    
    // User Table - column nmaes
    private static final String KEY_USER_USERNAME = "username";
    private static final String KEY_USER_PASSHASH = "passHash";
    private static final String KEY_USER_EMAIL = "email";
    
    // Queues Table - column names
    private static final String KEY_QUEUE_NAME = "name";
    private static final String KEY_QUEUE_TURN = "turn";
 
    // Participant Table - column names
    private static final String KEY_PARIICIPANT_NAME = "name";    
    
    // QUEUES_PARTICIPANTS Table - column names
    private static final String KEY_QUEUE_OID = "queue_oid";
    private static final String KEY_PARIICIPANT_OID = "participant_oid";    
    private static final String KEY_QUEUE_PARTICIPANT_TURN = "turn";
    private static final String KEY_QUEUE_PARTICIPANT_JUMPED_TO_PAY = "jumped_to_pay";
    
    // Table Create Statements
    // User table create statement
    private static final String CREATE_TABLE_USER = "CREATE TABLE "
            + TABLE_USER + "(" 
    		+ KEY_ID + " INTEGER PRIMARY KEY,"
    		+ KEY_OBJECTID + " TEXT,"
            + KEY_USER_USERNAME + " TEXT," 
    		+ KEY_USER_PASSHASH + " TEXT," 
            + KEY_USER_EMAIL + " TEXT" + ")";
 
    // Queue table create statement
    private static final String CREATE_TABLE_QUEUE = "CREATE TABLE " 
    		+ TABLE_QUEUES + "(" 
    		+ KEY_ID + " INTEGER PRIMARY KEY,"
    		+ KEY_OBJECTID + " TEXT,"
            + KEY_QUEUE_NAME + " TEXT,"
            + KEY_QUEUE_TURN + " INTEGER" + ")";
 
    // Participant table create statement
    private static final String CREATE_TABLE_PARIICIPANT = "CREATE TABLE " 
    		+ TABLE_PARTICIPANTS + "(" 
    		+ KEY_ID + " INTEGER PRIMARY KEY,"
    		+ KEY_OBJECTID + " TEXT,"
    		+ KEY_PARIICIPANT_NAME + " TEXT" + ")";
    
    // todo_tag table create statement
    private static final String CREATE_TABLE_QUEUES_PARTICIPANTS = "CREATE TABLE "
            + TABLE_QUEUES_PARTICIPANTS + "(" 
    		+ KEY_ID + " INTEGER PRIMARY KEY,"
    		+ KEY_OBJECTID + " TEXT,"
            + KEY_QUEUE_OID + " TEXT," 
            + KEY_PARIICIPANT_OID + " TEXT,"
            + KEY_QUEUE_PARTICIPANT_TURN + " INTEGER," 
            + KEY_QUEUE_PARTICIPANT_JUMPED_TO_PAY + " INTEGER" + ");";
 
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    public static DatabaseHelper getInstance(Context context) {
        if(instance == null) {
           instance = new DatabaseHelper(context);
        }
        return instance;
     }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
 
        // creating required tables
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_QUEUE);
        db.execSQL(CREATE_TABLE_PARIICIPANT);
        db.execSQL(CREATE_TABLE_QUEUES_PARTICIPANTS);
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUEUES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARTICIPANTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUEUES_PARTICIPANTS);
        
        // create new tables
        onCreate(db);
    }
    
    //CRUD Operations
    private synchronized long insert(String table, String nullColumnHack, ContentValues values){
    	SQLiteDatabase db = this.getWritableDatabase();
    	return db.insert(table, nullColumnHack, values);    	
    }

    private synchronized int update(String table, ContentValues values, String whereClause){
    	SQLiteDatabase db = this.getWritableDatabase();
    	return db.update(table, values, whereClause, null);    	
    }
    
    private synchronized int delete(String table, String whereClause){
    	SQLiteDatabase db = this.getWritableDatabase();
    	return db.delete(table, whereClause, null);    	
    }
    
    private Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy){
    	SQLiteDatabase db = this.getReadableDatabase();
    	return db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);    	
    }
    
    private Cursor rawQuery(String sql){
    	SQLiteDatabase db = this.getReadableDatabase();
    	return db.rawQuery(sql, null);
    }
    
    //Query Constructors
    private String queryConstructorSelection(String table, String selection){
    	return "SELECT " + selection + " FROM " + table;
    }
    
    private String queryConstructorAll(String table){
    	return queryConstructorSelection(table, "*");
    }
    
    private String queryConstructorAllWhere(String table, String where){
    	return queryConstructorAll(table) + " WHERE " + where;
    }
    
    private String queryConstructorWhereSelection(String table, String where, String selection){
    	return queryConstructorSelection(table, selection) + " WHERE " + where;
    }
    
    private String queryConstructorAllWhereOrderBy(String table, String where, String orderByField){
    	return queryConstructorAllWhere(table, where) + " ORDER BY " + orderByField;
    }
    
    public List<String> getObjectsId(String table){
    	List<String> result = new ArrayList<String>();
    	Cursor c = rawQuery(queryConstructorSelection(table, KEY_OBJECTID));
    	if (c != null && c.moveToFirst()) {    		
    		do{
    			result.add(c.getString(c.getColumnIndex(KEY_OBJECTID)));
    		}while(c.moveToNext());
    	}
    	return result;
    }
    
    public void queryUser(){
    	Cursor c = rawQuery(queryConstructorAll(TABLE_USER));
    	
    	Users user = Users.getInstance();
    	if (c != null && c.moveToFirst()) {    		
    		user.setObjectID(c.getString(c.getColumnIndex(KEY_OBJECTID)));
			user.setUsername(c.getString(c.getColumnIndex(KEY_USER_USERNAME)));
			user.setPassHash(c.getString(c.getColumnIndex(KEY_USER_PASSHASH)));
			user.setEmail(c.getString(c.getColumnIndex(KEY_USER_EMAIL)));
    		user.setUserLogged(true);
    	}else{
    		user.setUserLogged(false);
    	}
    	if(c != null && !c.isClosed()) c.close();
    }
    /**
     * @param objectId
     * @param username
     * @param passHash
     * @param email
     * @return the row ID of the newly inserted row, or -1 if an error occurred 
     */
    public long insertUser(String objectId, String username, String passHash, String email){
    	ContentValues values = new ContentValues();
    	values.put(KEY_OBJECTID, objectId);
    	values.put(KEY_USER_USERNAME, username);
    	values.put(KEY_USER_PASSHASH, passHash);
    	values.put(KEY_USER_EMAIL, email);
    	
    	long id = insert(TABLE_USER, null, values);
    	
    	Users user = Users.getInstance();
    	if(id != -1){
    		user.setObjectID(objectId);
			user.setUsername(username);
			user.setPassHash(passHash);
			user.setEmail(email);
    		user.setUserLogged(true);
    	}else{    		
    		user.setUserLogged(false);
    	}
    	
    	return id;
    }
    
    public Participants getParticipantByName(String name){
    	Cursor c = rawQuery(queryConstructorAllWhere(TABLE_PARTICIPANTS, KEY_PARIICIPANT_NAME + " Like '" + name + "'"));
    	Participants p = null;
    	if (c != null && c.moveToFirst()) {    		    	
			p = new Participants(
					c.getString(c.getColumnIndex(KEY_OBJECTID)), 
					c.getString(c.getColumnIndex(KEY_PARIICIPANT_NAME)));
    	}
    	return p;
    }
    
    /**
     * @return a list with all the participants information
     */
    public List<Participants> getAllParticipants(){
    	Cursor c = rawQuery(queryConstructorAll(TABLE_PARTICIPANTS));
    	List<Participants> participants = new ArrayList<Participants>();
    	
    	if (c != null && c.moveToFirst()) {    		
    		do{
    			Participants p = new Participants(
    					c.getString(c.getColumnIndex(KEY_OBJECTID)), 
    					c.getString(c.getColumnIndex(KEY_PARIICIPANT_NAME)));
    			participants.add(p);
    		}while(c.moveToNext());
    	}
    	if(c != null && !c.isClosed()) c.close();
    	
    	return participants;
    }
    
    public Cursor getParticipantsCursor(){
    	return rawQuery(queryConstructorAll(TABLE_PARTICIPANTS));    	    	
    }
    
    /**
     * @return a list with all the participants information
     */
    public List<Participants> getAllParticipantsSelection(String[] selectionList){
    	Cursor c = rawQuery(queryConstructorAll(TABLE_PARTICIPANTS));
    	List<Participants> participants = new ArrayList<Participants>();
    	
    	List<String> s = new ArrayList<String>();
    	for (String selection : selectionList) {
			s.add(selection);
		}
    	
    	if (c != null && c.moveToFirst()) {    		
    		do{
    			Participants p = new Participants();
    			if(s.contains(Participants.KEY_OBJCTIVEID)){
    				p.setObjectId(c.getString(c.getColumnIndex(KEY_OBJECTID)));
    			}
    			if(s.contains(Participants.KEY_NAME)){
    				p.setName(c.getString(c.getColumnIndex(KEY_PARIICIPANT_NAME)));
    			}    			
    			participants.add(p);
    		}while(c.moveToNext());
    	}
    	if(c != null && !c.isClosed()) c.close();
    	
    	return participants;
    }
    
    public Participants getParticipantByOID(String objectID){
    	Cursor c = rawQuery(queryConstructorAllWhere(TABLE_PARTICIPANTS, KEY_OBJECTID + " Like '" + objectID + "'"));
    	Participants p = null;
    	if (c != null && c.moveToFirst()) {
    		p = new Participants(objectID, c.getString(c.getColumnIndex(KEY_PARIICIPANT_NAME)));
    	}
    	if(c != null && !c.isClosed()) c.close();
    	return p;
    }
    
    public long insertParticipant(String objectId, String name){
    	ContentValues values = new ContentValues();
    	values.put(KEY_OBJECTID, objectId);
    	values.put(KEY_PARIICIPANT_NAME, name);

    	
    	return insert(TABLE_PARTICIPANTS, null, values);
    }
    
    public int updateParticipant(String objectId, String name){    	
    	ContentValues values = new ContentValues();    	
    	values.put(KEY_PARIICIPANT_NAME, name);
    	
    	return update(TABLE_PARTICIPANTS, values, KEY_OBJECTID + " Like '" + objectId + "'");    	
    }
    
    /**
     * 
     * @param objectId
     * @param name
     * @param turn
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise. To remove all rows and get a count pass "1" as the whereClause.
     */
    public int deleteParticipant(String objectId){    	    	
    	return delete(TABLE_PARTICIPANTS, KEY_OBJECTID + " Like '" + objectId + "'");    	
    }
    
    public long insertQueue(String objectId, String name, int turn){
    	ContentValues values = new ContentValues();
    	values.put(KEY_OBJECTID, objectId);
    	values.put(KEY_QUEUE_NAME, name);    	
    	values.put(KEY_QUEUE_TURN, turn);
    	
    	return insert(TABLE_QUEUES, null, values);
    }
    
    public long updateQueue(String objectId, String name, int turn){
    	ContentValues values = new ContentValues();    	
    	values.put(KEY_QUEUE_NAME, name);    	
    	values.put(KEY_QUEUE_TURN, turn);
    	
    	return update(TABLE_QUEUES, values, KEY_OBJECTID + " Like '" + objectId + "'");
    } 
    
    public int deleteQueue(String objectId){    	    	
    	return delete(TABLE_QUEUES, KEY_OBJECTID + " Like '" + objectId + "'");    	
    }
    
    /**
     * @return a list with all the queues information
     */
    public List<Queue> getAllQueues(){
    	Cursor c = rawQuery(queryConstructorAll(TABLE_QUEUES));
    	List<Queue> queues = new ArrayList<Queue>();
    	
    	if (c != null && c.moveToFirst()) {    		
    		do{
    			Queue q = new Queue(
    					c.getString(c.getColumnIndex(KEY_OBJECTID)), 
    					c.getString(c.getColumnIndex(KEY_QUEUE_NAME)),
    					c.getInt(c.getColumnIndex(KEY_QUEUE_TURN)));
    			queues.add(q);
    		}while(c.moveToNext());
    	}
    	if(c != null && !c.isClosed()) c.close();
    	
    	return queues;
    }
    
    public Cursor getQueuesCursor(){
    	return rawQuery(queryConstructorAll(TABLE_QUEUES));    	    	
    }
    
    public Queue getQueueById(long _id){
    	Cursor c = rawQuery(queryConstructorAllWhere(TABLE_QUEUES, KEY_ID + " = " + _id));
    	Queue q = null;
    	if (c != null && c.moveToFirst()) {
    		q = new Queue(
				c.getString(c.getColumnIndex(KEY_OBJECTID)), 
				c.getString(c.getColumnIndex(KEY_QUEUE_NAME)),
				c.getInt(c.getColumnIndex(KEY_QUEUE_TURN)));
    	}
    	return q; 
    }
    
    public Queue getQueueFromCursor(Cursor c){    	
    	return new Queue(
				c.getString(c.getColumnIndex(KEY_OBJECTID)), 
				c.getString(c.getColumnIndex(KEY_QUEUE_NAME)),
				c.getInt(c.getColumnIndex(KEY_QUEUE_TURN))); 
    }
    
    public QPRelate getQueuesParticipants(String queueOID, String participantOID){
    	QPRelate qp = null;
    	Cursor c = rawQuery(queryConstructorAllWhere(TABLE_QUEUES_PARTICIPANTS, KEY_QUEUE_OID + " Like '" + queueOID + "' AND " +
    			KEY_PARIICIPANT_OID + " Like '" + participantOID + "'"));
    	if (c != null && c.moveToFirst()) {
    		qp = new QPRelate(c.getString(c.getColumnIndex(KEY_OBJECTID)), 
    				c.getString(c.getColumnIndex(KEY_QUEUE_OID)), 
    				c.getString(c.getColumnIndex(KEY_PARIICIPANT_OID)), 
    				c.getInt(c.getColumnIndex(KEY_QUEUE_PARTICIPANT_TURN)), 
    				c.getInt(c.getColumnIndex(KEY_QUEUE_PARTICIPANT_JUMPED_TO_PAY)));
    	}
    	if(c != null && !c.isClosed()) c.close();
    	return qp;
    }
    
    public long insertQueuesParticipants(String objectId, String queueOID, String participantOID, int turn, int jumpedToPay){
    	ContentValues values = new ContentValues();
    	values.put(KEY_OBJECTID, objectId);
    	values.put(KEY_QUEUE_OID, queueOID);
    	values.put(KEY_PARIICIPANT_OID, participantOID);
    	values.put(KEY_QUEUE_PARTICIPANT_TURN, turn);
    	values.put(KEY_QUEUE_PARTICIPANT_JUMPED_TO_PAY, jumpedToPay);
    	
    	return insert(TABLE_QUEUES_PARTICIPANTS, null, values);
    }
    
    public long updateQueuesParticipants(String objectId, String queueOID, String participantOID, int turn, int jumpedToPay){
    	ContentValues values = new ContentValues();    	
    	values.put(KEY_QUEUE_OID, queueOID);
    	values.put(KEY_PARIICIPANT_OID, participantOID);
    	values.put(KEY_QUEUE_PARTICIPANT_TURN, turn);
    	values.put(KEY_QUEUE_PARTICIPANT_JUMPED_TO_PAY, jumpedToPay);
    	
    	return update(TABLE_QUEUES_PARTICIPANTS, values, KEY_OBJECTID + " Like '" + objectId + "'");
    }
    
    public int deleteQueuesParticipants(String objectId){    	    	
    	return delete(TABLE_QUEUES_PARTICIPANTS, KEY_OBJECTID + " Like '" + objectId + "'");    	
    }
    
    public List<Participants> getQueuePartipants(String queueOID){
    	List<Participants> participants = new ArrayList<Participants>();
    	
    	Cursor c = rawQuery(queryConstructorAllWhereOrderBy(TABLE_QUEUES_PARTICIPANTS, KEY_QUEUE_OID + " Like '" + queueOID + "'", KEY_QUEUE_PARTICIPANT_TURN));
    	if (c != null && c.moveToFirst()) {    		
    		do{
    			Participants p = getParticipantByOID(c.getString(c.getColumnIndex(KEY_PARIICIPANT_OID)));
    			if(p!=null){
    				participants.add(p);
    			}    			
    		}while(c.moveToNext());
    	}
    	if(c != null && !c.isClosed()) c.close();
    	return participants;
    }
}
