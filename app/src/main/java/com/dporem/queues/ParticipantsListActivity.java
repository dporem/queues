package com.dporem.queues;

import java.util.List;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dporem.queues.adapters.ParticipantListAdapter;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;

public class ParticipantsListActivity  extends ListActivity{
	@SuppressWarnings("unused")
	private final static String TAGLOG = ParticipantsListActivity.class.getSimpleName();
	
	private ParticipantListAdapter adapter;
	private DatabaseHelper db;
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equals(Constants.BR_PARTICIPANT_LIST_UPDATE))			
			{
				adapter.changeCursor(db.getParticipantsCursor());
			}			
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_participant_list);
		setTitle(this.getString(R.string.app_name) + "\\" + this.getString(R.string.menu_participant));
		
		registerBroadcastReceiverFilder(this);
		
		TextView empty = (TextView) findViewById(R.id.empty_list);		
		empty.setVisibility(View.GONE);
		
		db = DatabaseHelper.getInstance(this);		
		List<Participants> participants = db.getAllParticipants();
		if(participants.isEmpty()){
			empty.setVisibility(View.VISIBLE);	
		}
		
		adapter = new ParticipantListAdapter(this, db.getParticipantsCursor());
		setListAdapter(adapter);
		
		registerForContextMenu(getListView());
	}
	
	private void registerBroadcastReceiverFilder(Context mContext) {
    	IntentFilter listChangedFilter = new IntentFilter(Constants.BR_PARTICIPANT_LIST_UPDATE);    	    	
    	mContext.registerReceiver(mBroadcastReceiver, listChangedFilter);
    }
    
    @Override
    protected void onDestroy() {
    	this.unregisterReceiver(mBroadcastReceiver);
    	super.onDestroy();
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		menu.add(0, Constants.MENU_ID_INSERT_PARTICIPANT, 
				0, R.string.menu_insert_participant);
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case Constants.MENU_ID_INSERT_PARTICIPANT:
			Intent i = new Intent(this, InsertParticipantActivity.class);
			startActivity(i);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
