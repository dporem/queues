package com.dporem.queues;

import java.util.ArrayList;
import java.util.List;

import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.QPRelate;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.tasks.UpdateQPRelateTask;
import com.dporem.queues.tasks.UpdateQueueTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AdvanceQueue extends Activity {
	private List<Participants> participants;
	private Queue queue;
	private Context c;
	private DatabaseHelper db;
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equals(Constants.BR_ADVANCE_QUEUE_SUCCESS))			
			{																		
	            finish();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_advance_queue);
		
		c = this;
		
		registerBroadcastReceiverFilder(c);
		
		db = DatabaseHelper.getInstance(this);
		long _id = this.getIntent().getExtras().getLong(Constants.BUNDLE_CODE_ADVANCE_QUEUE_ACTIVITY, -1);
		queue = db.getQueueById(_id);
		
		if(queue != null){
			participants = db.getQueuePartipants(queue.getObjectId());
			boolean foundP = false;
			do{
				QPRelate qpRelate = db.getQueuesParticipants(queue.getObjectId(), participants.get(queue.getTurn()).getObjectId());
				if(qpRelate != null && qpRelate.getJumpedToPay() != 0){
					int nextTurn = queue.getTurn()+1;
					if(participants.size() == nextTurn){
						nextTurn = 0;
					}
					queue.setTurn(nextTurn);
					UpdateQPRelateTask updateQPRelateTask = new UpdateQPRelateTask(c, qpRelate, -1);
					updateQPRelateTask.execute();
				}else{
					foundP = true;
				}
			}while(!foundP);
			((TextView)findViewById(R.id.tvNextInTurn)).setText(participants.get(queue.getTurn()).getName());			
		}
		
		Button bPay = (Button)findViewById(R.id.bPay);
		bPay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				UpdateQueueTask updateQueueTask = new UpdateQueueTask(c, queue, participants.size());
				updateQueueTask.execute();				
			}
		});
		
		Button bPayOther = (Button)findViewById(R.id.bPayOther);
		bPayOther.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayDialog();
			}
		});
	}
	
	private void registerBroadcastReceiverFilder(Context mContext) {
    	IntentFilter successfulFilter = new IntentFilter(Constants.BR_ADVANCE_QUEUE_SUCCESS);    	
    	mContext.registerReceiver(mBroadcastReceiver, successfulFilter);    	
    }
	
	@Override
    protected void onDestroy() {
    	this.unregisterReceiver(mBroadcastReceiver);
    	super.onDestroy();
    }
	
	private void displayDialog(){
		List<String> participantNames = new ArrayList<String>();
		for (Participants p : participants) {
			if(p.getObjectId() != participants.get(queue.getTurn()).getObjectId()){
				participantNames.add(p.getName());
			}
		}
		
		final Dialog dialog = new Dialog(c);
		dialog.setContentView(R.layout.dialog_pay_other);
		dialog.setTitle(c.getString(R.string.dialog_pay_other));
		
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(c, android.R.layout.simple_list_item_1, participantNames);
		final Spinner sChooseParticipant = (Spinner) dialog.findViewById(R.id.sChooseParticipant);
		sChooseParticipant.setAdapter(spinnerAdapter);
		
		dialog.show();
		
		Button bPayOtherDialog = (Button) dialog.findViewById(R.id.bPayOtherDialog);
		bPayOtherDialog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String name = sChooseParticipant.getSelectedItem().toString();
				Participants part = null;
				for (Participants p : participants) {
					if(p.getName().equals(name)){
						part = p;
						break;
					}
				}
				if(part!=null){
					QPRelate qpRelate = db.getQueuesParticipants(queue.getObjectId(), part.getObjectId());
					if(qpRelate != null){						
						UpdateQPRelateTask updateQPRelateTask = new UpdateQPRelateTask(c, qpRelate, +1);
						updateQPRelateTask.execute();
					}
				}
				dialog.dismiss();
				finish();
			}		        	
        });
	}
}
