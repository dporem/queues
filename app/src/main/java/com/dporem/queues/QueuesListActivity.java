package com.dporem.queues;

import java.util.List;

import com.dporem.queues.adapters.ParticipantListAdapter;
import com.dporem.queues.adapters.QueueListAdapter;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.parseclasses.Users;
import com.dporem.queues.tasks.UpdateDBTask;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

public class QueuesListActivity extends ListActivity{
	
	private TextView empty;
	@SuppressWarnings("unused")
	private final static String TAGLOG = ParticipantsListActivity.class.getSimpleName();
	
	private QueueListAdapter adapter;
	private DatabaseHelper db;
	private Context c;
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equals(Constants.BR_QUEUE_LIST_UPDATE))			
			{
				adapter.changeCursor(db.getQueuesCursor());
				if(adapter.getCount()>0){
					empty.setVisibility(View.GONE);
				}else{
					empty.setVisibility(View.VISIBLE);
				}				
			}else if(intent.getAction().equals(Constants.BR_UPDATE_DB_FINISHED)){
				setProgressBarIndeterminate(false);
				setProgressBarIndeterminateVisibility(false);
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS) ;
		
		setContentView(R.layout.activity_queue_list);		 
		setProgressBarIndeterminate(true);
		setProgressBarIndeterminateVisibility(true);
		
		c = this;
		registerBroadcastReceiverFilder(this);
		
		new UpdateDBTask(this).execute();
		
		empty = (TextView) findViewById(R.id.empty_list);				
		empty.setVisibility(View.GONE);				
		
		db = DatabaseHelper.getInstance(this);
		List<Queue> queues = db.getAllQueues();
		if(queues.isEmpty()){
			empty.setVisibility(View.VISIBLE);	
		}
		
		adapter = new QueueListAdapter(this, db.getQueuesCursor());
		setListAdapter(adapter);
		
		registerForContextMenu(getListView());
	}
	
	private void registerBroadcastReceiverFilder(Context mContext) {
    	IntentFilter listChangedFilter = new IntentFilter(Constants.BR_QUEUE_LIST_UPDATE);    	    	
    	IntentFilter updateDBFinishedFilter = new IntentFilter(Constants.BR_UPDATE_DB_FINISHED);
    	
    	mContext.registerReceiver(mBroadcastReceiver, listChangedFilter);
    	mContext.registerReceiver(mBroadcastReceiver, updateDBFinishedFilter);
    }	
	
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		menu.add(0, Constants.MENU_ID_PARTICIPANT, 
				0, R.string.menu_participant);
		if(!db.getAllParticipants().isEmpty()){
			menu.add(0, Constants.MENU_ID_INSERT_QUEUE, 
					0, R.string.menu_insert_queue);
		}
		return result;
	}
	*/
	
	@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, Constants.MENU_ID_PARTICIPANT, 
				0, R.string.menu_participant);
		if(!db.getAllParticipants().isEmpty()){
			menu.add(0, Constants.MENU_ID_INSERT_QUEUE, 
					0, R.string.menu_insert_queue);
		}
        return super.onPrepareOptionsMenu(menu);
    }

	@Override
    protected void onDestroy() {
    	this.unregisterReceiver(mBroadcastReceiver);
    	super.onDestroy();
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i = null;
		switch (item.getItemId()) {
		case Constants.MENU_ID_PARTICIPANT:
			i = new Intent(this, ParticipantsListActivity.class);
			startActivity(i);
			return true;
		case Constants.MENU_ID_INSERT_QUEUE:
			i = new Intent(this, InsertQueueActivity.class);
			startActivity(i);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Bundle bundle = new Bundle();
		bundle.putLong(Constants.BUNDLE_CODE_ADVANCE_QUEUE_ACTIVITY, id);
		Intent i = new Intent(c, AdvanceQueue.class);
		i.putExtras(bundle);
		//i.putExtra(Constants.BUNDLE_CODE_ADVANCE_QUEUE_ACTIVITY, id);
		startActivity(i);
		
		super.onListItemClick(l, v, position, id);
	}
}
