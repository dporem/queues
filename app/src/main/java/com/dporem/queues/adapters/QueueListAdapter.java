package com.dporem.queues.adapters;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.dporem.queues.R;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.QPRelate;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.tasks.UpdateQPRelateTask;

public class QueueListAdapter extends CursorAdapter{
	
	private LayoutInflater mLayoutInflater;	
	private Context mContext;
	
	public QueueListAdapter(Context context, Cursor c) {
		super(context, c, 0);
		mLayoutInflater = LayoutInflater.from(context);
		this.mContext = context;
	}
	
	@Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = mLayoutInflater.inflate(R.layout.list_row_queue_list, parent, false);
        return v;
    }
	
	@Override
    public void bindView(View v, Context context, Cursor c) {		
		String composedName = c.getString(c.getColumnIndex(Queue.KEY_NAME));
		
		TextView queueName = (TextView) v.findViewById(R.id.tvQueueName);
		queueName.setText(composedName.substring(0, composedName.indexOf(" - ")));
		
		TextView tvParticipants = (TextView) v.findViewById(R.id.tvParticipants);
		tvParticipants.setText(composedName.substring(composedName.indexOf(" - ")+3));
		
		TextView tvNextParticipant = (TextView) v.findViewById(R.id.tvNextParticipant);
		tvNextParticipant.setText(mContext.getString(R.string.list_indication_next) + " " + getNextInQueue(c));
	}	
	
	
	
	private String getNextInQueue(Cursor c){
		DatabaseHelper db = DatabaseHelper.getInstance(mContext);
		Queue queue = db.getQueueFromCursor(c);
		List<Participants> participants = db.getQueuePartipants(queue.getObjectId());
		boolean foundP = false;
		do{
			QPRelate qpRelate = db.getQueuesParticipants(queue.getObjectId(), participants.get(queue.getTurn()).getObjectId());
			if(qpRelate != null && qpRelate.getJumpedToPay() != 0){
				int nextTurn = queue.getTurn()+1;
				if(participants.size() == nextTurn){
					nextTurn = 0;
				}
				queue.setTurn(nextTurn);
				UpdateQPRelateTask updateQPRelateTask = new UpdateQPRelateTask(mContext, qpRelate, -1);
				updateQPRelateTask.execute();
			}else{
				foundP = true;
			}
		}while(!foundP);
		return participants.get(queue.getTurn()).getName();
	}
}