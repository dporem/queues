package com.dporem.queues.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.dporem.queues.R;
import com.dporem.queues.parseclasses.Participants;

public class ParticipantListAdapter extends CursorAdapter{
	
	private LayoutInflater mLayoutInflater;	
	
	public ParticipantListAdapter(Context context, Cursor c) {
		super(context, c, 0);
		mLayoutInflater = LayoutInflater.from(context);
	}
	
	@Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = mLayoutInflater.inflate(R.layout.list_row_participant_list, parent, false);
        return v;
    }
	
	@Override
    public void bindView(View v, Context context, Cursor c) {
		TextView participantName = (TextView) v.findViewById(R.id.tvParticipantName);
		participantName.setText(c.getString(c.getColumnIndex(Participants.KEY_NAME)));
	}	
}
