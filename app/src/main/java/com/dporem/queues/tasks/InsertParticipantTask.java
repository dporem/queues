package com.dporem.queues.tasks;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dporem.queues.R;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.Users;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class InsertParticipantTask extends AsyncTask<Void, Void, Void>{
	private ProgressDialog pd;
	private Context c;
	private String participantName;
	private List<ParseObject> participantList;
	private ParseObject participants;
	private boolean triedToSave = false;
	private boolean hadException = false;
	
	public InsertParticipantTask(Context c, String participantName) {
		this.c = c;
		this.participantName = participantName;
	}

	@Override
	protected void onPreExecute() {
		pd = new ProgressDialog(c);
		pd.setTitle(c.getResources().getString(R.string.inserting_participant));
		pd.setMessage(c.getResources().getString(R.string.please_wait));
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			Users user = Users.getInstance();
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_PARTICIPANT);
			query.whereEqualTo(Participants.KEY_NAME, participantName);
			query.whereEqualTo(Participants.KEY_USEROID, user.getObjectID());
			
			participantList = query.find();
			if(participantList.size()>0){
				return null;
			}
			
			participants = new ParseObject(Constants.PARSE_CLASSNAME_PARTICIPANT);
			participants.put(Participants.KEY_NAME, participantName);
			participants.put(Participants.KEY_USEROID, user.getObjectID());
			participants.put(Participants.KEY_DELETED, false);
		
			participants.save();
			triedToSave = true;
			
			//participantList = query.find();
			//if(participantList.size()>0){
				DatabaseHelper db = DatabaseHelper.getInstance(c);
				if(db.insertParticipant(participants.getObjectId(), participantName) == -1){
					//participantList.removeAll(null);
					return null;
				}
			//}
		} catch (ParseException e) {
			hadException = true;
			//e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		if (pd!=null && pd.isShowing()) {
    		pd.dismiss();

    	}
		
		if(participantList.size()==0 && triedToSave){			
			Intent intent = new Intent(Constants.BR_PARTICIPANT_INSERT_SUCCESSFUL);
            c.sendBroadcast(intent);			
            
		}else if(participants.getObjectId() == null || hadException){
			Intent intent = new Intent(Constants.BR_PARTICIPANT_INSERT_UNSUCCESSFUL);			
            c.sendBroadcast(intent);
            
		}else{
			Intent intent = new Intent(Constants.BR_PARTICIPANT_INSERT_ALREADY_EXISTS);			
            c.sendBroadcast(intent);
		}
		
		super.onPostExecute(result);
	}
}
