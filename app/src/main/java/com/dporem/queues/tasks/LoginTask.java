package com.dporem.queues.tasks;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dporem.queues.R;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Users;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class LoginTask extends AsyncTask<Void, Void, Void>{

	private ProgressDialog pd;
	private List<ParseObject> usersList;
	private Context c;
	
	public LoginTask(Context c){
		this.c = c;
	}
	
	@Override
	protected void onPreExecute() {
		pd = new ProgressDialog(c);
		pd.setTitle(c.getResources().getString(R.string.login_in));
		pd.setMessage(c.getResources().getString(R.string.please_wait));
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_USER);
		query.whereEqualTo(Users.KEY_USERNAME, "funambol");
		//query.whereEqualTo(Users.KEY_USERNAME, "admin");
		query.whereEqualTo(Users.KEY_DELETED, false);
		try {
			usersList = query.find();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		if (pd!=null && pd.isShowing()) {
    		pd.dismiss();

    	}
		
		if(usersList != null && usersList.size() == 1){			
			Users user = Users.getInstance();			
			user.setObjectID(String.valueOf(usersList.get(0).getObjectId()));
			user.setUsername(String.valueOf(usersList.get(0).get(Users.KEY_USERNAME)));
			user.setPassHash(String.valueOf(usersList.get(0).get(Users.KEY_PASSHASH)));
			user.setEmail(String.valueOf(usersList.get(0).get(Users.KEY_EMAIL)));
			user.setUserLogged(true);			
			
			DatabaseHelper db = DatabaseHelper.getInstance(c);
			long id = db.insertUser(user.getObjectID(), user.getUsername(), user.getPassHash(), user.getEmail());
			if(id == -1){
				Intent intent = new Intent(Constants.BR_LOGIN_UNSUCCESSFUL);			
	            c.sendBroadcast(intent);
				user.setUserLogged(false);
			}else{
				Intent intent = new Intent(Constants.BR_LOGIN_SUCCESSFUL);			
	            c.sendBroadcast(intent);	
			}						
		}else{
			Users user = Users.getInstance();
			user.setUserLogged(false);
			
			Intent intent = new Intent(Constants.BR_LOGIN_UNSUCCESSFUL);			
            c.sendBroadcast(intent);
		}
		
		super.onPostExecute(result);
	}

}
