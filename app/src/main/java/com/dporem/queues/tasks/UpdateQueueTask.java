package com.dporem.queues.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.dporem.queues.R;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.parseclasses.Users;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class UpdateQueueTask extends AsyncTask<Void, Void, Void>{
	private ProgressDialog pd;
	private Context c;
	private Queue queue;
	private int maxTurn;
	private int nextTurn = -1;
	
	public UpdateQueueTask(Context c, Queue queue, int maxTurn) {
		this.c = c;
		this.queue = queue;
		this.maxTurn = maxTurn;
	}
	
	@Override
	protected void onPreExecute() {
		pd = new ProgressDialog(c);
		pd.setTitle(c.getResources().getString(R.string.updating_queue));
		pd.setMessage(c.getResources().getString(R.string.please_wait));
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			Users user = Users.getInstance();
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_QUEUE);			
			query.whereEqualTo(Queue.KEY_USEROID, user.getObjectID());
			query.whereEqualTo(Queue.KEY_OBJCTIVEID, queue.getObjectId());
			query.whereEqualTo(Queue.KEY_DELETED, false);			
			if(query.find().size()==0){
				return null;
			}			
			
			nextTurn = queue.getTurn()+1;
			if(maxTurn == nextTurn){
				nextTurn = 0;
			}
			
			query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_QUEUE);
			query.getInBackground(queue.getObjectId(), new GetCallback<ParseObject>() {
				public void done(ParseObject pq, ParseException e) {
					try {	
						if (e == null) {
							pq.put(Queue.KEY_TURN, nextTurn);						
							pq.save();
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}					
				}
			});
			
			DatabaseHelper db = DatabaseHelper.getInstance(c);
			if(db.updateQueue(queue.getObjectId(), queue.getName(), nextTurn)==0){
				return null;		
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		if (pd!=null && pd.isShowing()) {
    		pd.dismiss();

    	}
		
		Intent intent = new Intent(Constants.BR_ADVANCE_QUEUE_SUCCESS);			
        c.sendBroadcast(intent);
		
		//	Intent intent = new Intent(Constants.BR_QUEUE_PARTICIPANT_INSERT_SUCCESSFUL);			
		//    c.sendBroadcast(intent);            		
		
		super.onPostExecute(result);
	}
}
