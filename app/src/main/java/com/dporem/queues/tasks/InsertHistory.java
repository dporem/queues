package com.dporem.queues.tasks;

import android.os.AsyncTask;

import com.dporem.queues.parseclasses.History;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.Users;
import com.parse.ParseException;
import com.parse.ParseObject;

public class InsertHistory extends AsyncTask<Void, Void, Void>{

	private ParseObject history;
	private String actionPerformed;
	private String queueOID;
	
	public InsertHistory(String actionPerformed, String queueOID){
		this.actionPerformed = actionPerformed;
		this.queueOID = queueOID;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		Users user = Users.getInstance();
		
		try {
			
			history = new ParseObject("History");
			history.put(History.KEY_ACTION_PERFORMED, actionPerformed);
			history.put(History.KEY_USEROID, user.getObjectID());
			history.put(History.KEY_QUEUEOID, queueOID);
			history.put(Participants.KEY_DELETED, false);
		
			history.save();
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
