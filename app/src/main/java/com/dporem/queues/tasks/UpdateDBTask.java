package com.dporem.queues.tasks;

import java.util.ArrayList;
import java.util.List;

import com.dporem.queues.QueuesListActivity;
import com.dporem.queues.R;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.QPRelate;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.parseclasses.Users;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;

public class UpdateDBTask extends AsyncTask<Void, Void, Void>{

	private Context c;
	
	public UpdateDBTask(Context c){
		this.c = c;
	}
		
	@Override
	protected void onPreExecute() {
		showNotification();
		super.onPreExecute();
	}
	
	@Override
	protected void onPostExecute(Void result) {
		cancelNotification();
		super.onPostExecute(result);
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void showNotification(){
		// intent triggered, you can add other intent for other actions
        Intent intent = new Intent(c, QueuesListActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(c, 0, intent, 0);

        // this is it, we'll build the notification!
        // in the addAction method, if you don't want any icon, just set the first param to 0
        
        Builder builder = new Notification.Builder(c)
            .setContentTitle(c.getString(R.string.app_name))
            .setContentText(c.getString(R.string.notification_update))
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentIntent(pIntent)
            .setOngoing(true);                       

        Notification mNotification = null;
        if (Build.VERSION.SDK_INT>15) {
        	mNotification = builder.build();	
		}else{
			mNotification = builder.getNotification();
		}        
        
        mNotification.flags |= Notification.FLAG_NO_CLEAR;        
        
        NotificationManager notificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, mNotification);		
	}		
	
	private void cancelNotification(){
		if (Context.NOTIFICATION_SERVICE!=null) {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) c.getSystemService(ns);
            nMgr.cancel(0);
        }
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		updateParticipants();
		
		updateQueues();			
		
		Intent intent = new Intent(Constants.BR_UPDATE_DB_FINISHED);			
        c.sendBroadcast(intent);
        
		return null;
	}

	private void updateQP() {
		Users user = Users.getInstance();
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_QP_RELATE);
		query.whereEqualTo(Participants.KEY_USEROID, user.getObjectID());
        query.setLimit(10000);

		List<ParseObject> qp_relates = new ArrayList<ParseObject>();
		try {
			qp_relates = query.find();
		}catch (Exception e){
			
		}
		
		DatabaseHelper db = DatabaseHelper.getInstance(c);
		List<String> objectsId  = db.getObjectsId(DatabaseHelper.TABLE_QUEUES_PARTICIPANTS);
		
		if(qp_relates!= null && !qp_relates.isEmpty()){
			for (ParseObject qp_relate : qp_relates) {
				if(!objectsId.contains(qp_relate.getObjectId())){
					db.insertQueuesParticipants(qp_relate.getObjectId(), 
							(String) qp_relate.get(QPRelate.KEY_QUEUE_OID), (String) qp_relate.get(QPRelate.KEY_PARTICIPANT_OID), 
							(Integer)qp_relate.get(QPRelate.KEY_TURN), (Integer)qp_relate.get(QPRelate.KEY_JUMPEDTOPAY));
				}else{
					db.updateQueuesParticipants(qp_relate.getObjectId(), 
							(String) qp_relate.get(QPRelate.KEY_QUEUE_OID), (String) qp_relate.get(QPRelate.KEY_PARTICIPANT_OID), 
							(Integer)qp_relate.get(QPRelate.KEY_TURN), (Integer)qp_relate.get(QPRelate.KEY_JUMPEDTOPAY));
					
					objectsId.remove(qp_relate.getObjectId());
				}
			}
		}
		
		for (String objectId : objectsId) {
			db.deleteQueuesParticipants(objectId);
		}
		
	}

	private void updateQueues() {		
		Users user = Users.getInstance();
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_QUEUE);
		query.whereEqualTo(Participants.KEY_USEROID, user.getObjectID());
		query.setLimit(10000);
		
		List<ParseObject> queues = new ArrayList<ParseObject>();
		try {
			queues = query.find();
		}catch (Exception e){
			
		}
		
		DatabaseHelper db = DatabaseHelper.getInstance(c);
		List<String> objectsId  = db.getObjectsId(DatabaseHelper.TABLE_QUEUES);
		
		if(queues!= null && !queues.isEmpty()){
			for (ParseObject queue : queues) {
				if(!objectsId.contains(queue.getObjectId())){
					db.insertQueue(queue.getObjectId(), 
							(String) queue.get(Queue.KEY_NAME), (Integer)queue.get(Queue.KEY_TURN));
				}else{
					db.updateQueue(queue.getObjectId(), 
							(String) queue.get(Participants.KEY_NAME), (Integer)queue.get(Queue.KEY_TURN));
					
					objectsId.remove(queue.getObjectId());
				}
			}
		}
		
		for (String objectId : objectsId) {
			db.deleteQueue(objectId);
		}		
		
		updateQP();
		
		Intent i = new Intent(Constants.BR_QUEUE_LIST_UPDATE);			
        c.sendBroadcast(i);
	}

	private void updateParticipants() {
		Users user = Users.getInstance();
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_PARTICIPANT);
		query.whereEqualTo(Participants.KEY_USEROID, user.getObjectID());
        query.setLimit(10000);
        
		List<ParseObject> participants = new ArrayList<ParseObject>();
		try {
			participants = query.find();
		}catch (Exception e){
			
		}
		
		DatabaseHelper db = DatabaseHelper.getInstance(c);
		List<String> objectsId  = db.getObjectsId(DatabaseHelper.TABLE_PARTICIPANTS);
		
		if(participants!= null && !participants.isEmpty()){
			for (ParseObject participant : participants) {
				if(!objectsId.contains(participant.getObjectId())){
					db.insertParticipant(participant.getObjectId(), 
							(String) participant.get(Participants.KEY_NAME));
				}else{
					db.updateParticipant(participant.getObjectId(), 
							(String) participant.get(Participants.KEY_NAME));
					
					objectsId.remove(participant.getObjectId());
				}
			}
		}
		
		for (String objectId : objectsId) {
			db.deleteParticipant(objectId);
		}		
		
		Intent i = new Intent(Constants.BR_PARTICIPANT_LIST_UPDATE);			
        c.sendBroadcast(i);
	}
}
