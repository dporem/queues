package com.dporem.queues.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.QPRelate;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class UpdateQPRelateTask extends AsyncTask<Void, Void, Void>{
	private ProgressDialog pd;
	private Context c;
	private QPRelate qpRelate;
	private int difference = 0;
	
	public UpdateQPRelateTask(Context c, QPRelate qpRelate, int difference) {
		this.c = c;
		this.qpRelate = qpRelate;
		this.difference = difference;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
			
		DatabaseHelper db = DatabaseHelper.getInstance(c);
		db.updateQueuesParticipants(qpRelate.getObjectId(), qpRelate.getQueueOID(), 
				qpRelate.getParticipantOID(), qpRelate.getTurn(), (qpRelate.getJumpedToPay()+difference));		
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_QP_RELATE);
		query.getInBackground(qpRelate.getObjectId(), new GetCallback<ParseObject>() {
			public void done(ParseObject pq, ParseException e) {
				try {	
					if (e == null) {
						pq.put(QPRelate.KEY_JUMPEDTOPAY, qpRelate.getJumpedToPay()+difference);						
						pq.save();
					}
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}					
			}
		});
	
		return null;
	}
}
