package com.dporem.queues.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.dporem.queues.R;
import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.parseclasses.QPRelate;
import com.dporem.queues.parseclasses.Queue;
import com.dporem.queues.parseclasses.Users;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class InsertQueueTask extends AsyncTask<Void, Void, Void>{
	private List<String> participants;
	private ProgressDialog pd;
	private Context c;	
	private boolean hadException = false;
	private boolean alreadyPresent = false;
	private String queueName;
	
	public InsertQueueTask(Context c, List<String> participants, String queueName) {
		this.c = c;
		this.participants = participants;
		this.queueName = queueName;
	}

	@Override
	protected void onPreExecute() {
		pd = new ProgressDialog(c);
		pd.setTitle(c.getResources().getString(R.string.inserting_queue));
		pd.setMessage(c.getResources().getString(R.string.please_wait));
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			Users user = Users.getInstance();
			
			//Create Queue
			queueName = composeQueueName();	
											
			ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_CLASSNAME_QUEUE);
			query.whereEqualTo(Queue.KEY_NAME, queueName);
			query.whereEqualTo(Queue.KEY_USEROID, user.getObjectID());
			query.whereEqualTo(Queue.KEY_DELETED, false);
			if(query.find().size()>0){
				alreadyPresent = true;
				return null;
			}
			
			ParseObject queue = new ParseObject(Constants.PARSE_CLASSNAME_QUEUE);			
			queue.put(Queue.KEY_NAME, queueName);
			queue.put(Queue.KEY_USEROID, user.getObjectID());
			queue.put(Queue.KEY_DELETED, false);
			queue.put(Queue.KEY_TURN, 0);
			
			queue.save();
			
			DatabaseHelper db = DatabaseHelper.getInstance(c);
			db.insertQueue(queue.getObjectId(), queueName, 0);
			//List<ParseObject> queuesObject = new ArrayList<ParseObject>();			
			//queuesObject = query.find();
			
			
			/*List<String> objectsId  = db.getObjectsId(DatabaseHelper.TABLE_QUEUES);
			if(queuesObject!= null && !queuesObject.isEmpty()){
				for (ParseObject q : queuesObject) {
					if(objectsId.contains(q.getObjectId())){
						queuesObject.remove(q);
					}
				}
			}
			
			String queueOID = null;
			if(queuesObject.size()>0){
				queueOID = queuesObject.get(0).getObjectId();
				db.insertQueue(queueOID, queueName);
			}*/
			
			//if(queueOID != null){
			if(queue.getObjectId() != null){
				//Get OID of Participants
				List<String> participantsOID = new ArrayList<String>();
				for (String name : participants) {
					participantsOID.add(db.getParticipantByName(name).getObjectId());
				}
				
				if(!participantsOID.isEmpty()){									
					long seed = System.nanoTime();
					Collections.shuffle(participantsOID, new Random(seed));
					
					int turn = 0;
					for (String pOID : participantsOID) {
						ParseObject qp_relate = new ParseObject(Constants.PARSE_CLASSNAME_QP_RELATE);
						qp_relate.put(QPRelate.KEY_QUEUE_OID, queue.getObjectId());
						qp_relate.put(QPRelate.KEY_PARTICIPANT_OID, pOID);
						qp_relate.put(QPRelate.KEY_USEROID, user.getObjectID());						
						qp_relate.put(QPRelate.KEY_JUMPEDTOPAY, 0);
						qp_relate.put(QPRelate.KEY_TURN, turn);
												
						qp_relate.save();
						if(qp_relate.getObjectId() != null){
							db.insertQueuesParticipants(qp_relate.getObjectId(), queue.getObjectId(), pOID, turn, 0);
						}
						turn++;
					}

				}else{
					hadException = true;
				}
			}else{
				hadException = true;
			}
			
		} catch (ParseException e) {
			hadException = true;
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		if (pd!=null && pd.isShowing()) {
    		pd.dismiss();

    	}		
		if(alreadyPresent){
			Intent intent = new Intent(Constants.BR_QUEUE_PARTICIPANT_INSERT_ALREADY_EXISTS);
            c.sendBroadcast(intent);
            
		}else if(hadException){			
			Intent intent = new Intent(Constants.BR_QUEUE_PARTICIPANT_INSERT_UNSUCCESSFUL);
            c.sendBroadcast(intent);			
            
		}else {
			Intent intent = new Intent(Constants.BR_QUEUE_PARTICIPANT_INSERT_SUCCESSFUL);			
            c.sendBroadcast(intent);
            
		}
		
		super.onPostExecute(result);
	}
	
	private String composeQueueName(){
		if(queueName.isEmpty()){
			queueName = participants.size() + " " + c.getResources().getString(R.string.number_participants) + " ";
		}else{
			queueName = queueName + "(" + participants.size() + ") - ";
		}
		
		for (String name : participants) {
			queueName = queueName + name + ", ";
		}
		queueName = queueName.substring(0, queueName.lastIndexOf(", "));
		return queueName;
	}
}
