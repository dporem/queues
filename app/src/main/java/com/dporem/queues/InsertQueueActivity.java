package com.dporem.queues;

import java.util.ArrayList;
import java.util.List;

import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.parseclasses.Participants;
import com.dporem.queues.tasks.InsertQueueTask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class InsertQueueActivity extends Activity{
	private Context mContext;
	private ListView lvParticipants;
	private ArrayAdapter<String> listAdapter;
	private List<String> participants;
	private Spinner sAddParticipant;
	private List<String> participantNames;
	private ArrayAdapter<String> spinnerAdapter;
	private EditText etQueueName; 
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				
				if(intent.getAction().equals(Constants.BR_QUEUE_PARTICIPANT_INSERT_SUCCESSFUL))			
				{
					Intent i = new Intent(Constants.BR_QUEUE_LIST_UPDATE);			
		            mContext.sendBroadcast(i);
		            
					Toast.makeText(context, getResources().getString(R.string.insert_successful), Toast.LENGTH_LONG).show();															
		            finish();
				}else if(intent.getAction().equals(Constants.BR_QUEUE_PARTICIPANT_INSERT_UNSUCCESSFUL))			
				{
					Toast.makeText(context, getResources().getString(R.string.insert_unsuccessful), Toast.LENGTH_LONG).show();
				}else if(intent.getAction().equals(Constants.BR_QUEUE_PARTICIPANT_INSERT_ALREADY_EXISTS))			
				{
					Toast.makeText(context, getResources().getString(R.string.insert_queue_already_exists), Toast.LENGTH_LONG).show();
				}
			}
	};
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_queues);               
        mContext = this;
        registerBroadcastReceiverFilder(this);                
        
        DatabaseHelper db = DatabaseHelper.getInstance(mContext);
        participantNames = new ArrayList<String>();
        
        for (Participants participant : db.getAllParticipantsSelection(new String[]{Participants.KEY_NAME})) {
			participantNames.add(participant.getName());
		}
        
        etQueueName = (EditText) findViewById(R.id.etQueueName);
        
        spinnerAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, participantNames);        
        sAddParticipant = (Spinner) findViewById(R.id.sAddParticipant);
        sAddParticipant.setAdapter(spinnerAdapter);
        
        participants = new ArrayList<String>();
        
        listAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, participants);
        lvParticipants = (ListView) findViewById(R.id.lvParticipants);
        lvParticipants.setAdapter(listAdapter);
        
        Button bAddParticipant = (Button) findViewById(R.id.bAddParticipant);
        bAddParticipant.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
				participants.add(sAddParticipant.getSelectedItem().toString());
				participantNames.remove(sAddParticipant.getSelectedItem().toString());
				
				listAdapter.notifyDataSetChanged();
				spinnerAdapter.notifyDataSetChanged();
			}
		});
        
        Button bSaveQueue = (Button)findViewById(R.id.bSaveQueue);
        bSaveQueue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(participants==null || participants.isEmpty()){
					Toast.makeText(mContext, mContext.getString(R.string.no_participants_added), Toast.LENGTH_LONG).show();
				}else{
					new InsertQueueTask(mContext, participants, etQueueName.getText().toString()).execute();
				}
			}
		});
	}
	
	private void registerBroadcastReceiverFilder(Context mContext) {
    	IntentFilter successfulFilter = new IntentFilter(Constants.BR_QUEUE_PARTICIPANT_INSERT_SUCCESSFUL);    	
    	IntentFilter unsuccessfulFilter = new IntentFilter(Constants.BR_QUEUE_PARTICIPANT_INSERT_UNSUCCESSFUL);
    	IntentFilter alreadyExistsfulFilter = new IntentFilter(Constants.BR_QUEUE_PARTICIPANT_INSERT_ALREADY_EXISTS);
    	mContext.registerReceiver(mBroadcastReceiver, successfulFilter);
    	mContext.registerReceiver(mBroadcastReceiver, unsuccessfulFilter);
    	mContext.registerReceiver(mBroadcastReceiver, alreadyExistsfulFilter);
    }
	
	@Override
    protected void onDestroy() {
    	this.unregisterReceiver(mBroadcastReceiver);
    	super.onDestroy();
    }
}
