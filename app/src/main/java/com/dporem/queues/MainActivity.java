package com.dporem.queues;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.dporem.queues.misc.Constants;
import com.dporem.queues.misc.DatabaseHelper;
import com.dporem.queues.misc.Log;
import com.dporem.queues.parseclasses.Users;
import com.dporem.queues.tasks.LoginTask;
import com.parse.Parse;

public class MainActivity extends Activity {

	private Context c;
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equals(Constants.BR_LOGIN_SUCCESSFUL))			
			{
				
				Intent i = new Intent(c, QueuesListActivity.class);
				startActivity(i);
			}else if(intent.getAction().equals(Constants.BR_LOGIN_UNSUCCESSFUL)){
				
				Toast.makeText(context, getResources().getString(R.string.login_unsuccessful), Toast.LENGTH_LONG).show();
			}
			
		}
	};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.getInstance(this);
        DatabaseHelper db = DatabaseHelper.getInstance(this);
        db.queryUser();
        if(Users.getInstance().isUserLogged()){
        	Intent i = new Intent(this, QueuesListActivity.class);
        	startActivity(i);
        }
        setContentView(R.layout.activity_main);
        
        c = this;
        
        Parse.initialize(this, Constants.PARSE_APPLICATIONID, 
        		Constants.PARSE_CLIENTKEY);
		
        registerBroadcastReceiverFilder(this);
        
        Button bLogin = (Button)findViewById(R.id.bLogin);
        bLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				new LoginTask(c).execute();
			}
		});              
    }

    private void registerBroadcastReceiverFilder(Context mContext) {
    	IntentFilter successfulFilter = new IntentFilter(Constants.BR_LOGIN_SUCCESSFUL);    	
    	IntentFilter unsuccessfulFilter = new IntentFilter(Constants.BR_LOGIN_SUCCESSFUL);
    	mContext.registerReceiver(mBroadcastReceiver, successfulFilter);
    	mContext.registerReceiver(mBroadcastReceiver, unsuccessfulFilter);
    }
    
    @Override
    protected void onDestroy() {
    	this.unregisterReceiver(mBroadcastReceiver);
    	super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
