package com.dporem.queues;

import com.dporem.queues.misc.Constants;
import com.dporem.queues.tasks.InsertParticipantTask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertParticipantActivity extends Activity{
	private Context mContext;
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				
				if(intent.getAction().equals(Constants.BR_PARTICIPANT_INSERT_SUCCESSFUL))			
				{
					Intent i = new Intent(Constants.BR_PARTICIPANT_LIST_UPDATE);			
		            mContext.sendBroadcast(i);
		            
					Toast.makeText(context, getResources().getString(R.string.insert_successful), Toast.LENGTH_LONG).show();															
		            finish();
				}else if(intent.getAction().equals(Constants.BR_PARTICIPANT_INSERT_UNSUCCESSFUL))			
				{
					Toast.makeText(context, getResources().getString(R.string.insert_unsuccessful), Toast.LENGTH_LONG).show();
				}else if(intent.getAction().equals(Constants.BR_PARTICIPANT_INSERT_ALREADY_EXISTS))			
				{
					Toast.makeText(context, getResources().getString(R.string.insert_participant_already_exists), Toast.LENGTH_LONG).show();
				}
			}
	};
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_participant);   
        
        mContext = this;
        	
        registerBroadcastReceiverFilder(mContext);
        
        Button bInsert = (Button) findViewById(R.id.bInsert);
        bInsert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String name = ((EditText)findViewById(R.id.etName)).getText().toString();
				
				if(name.isEmpty()){
					Toast.makeText(mContext, getResources().getString(R.string.insert_participant_name), Toast.LENGTH_LONG).show();	
				}else{
					new InsertParticipantTask(mContext, name).execute();
				}
				
			}
		});
	}
	
	private void registerBroadcastReceiverFilder(Context mContext) {
    	IntentFilter successfulFilter = new IntentFilter(Constants.BR_PARTICIPANT_INSERT_SUCCESSFUL);    	
    	IntentFilter unsuccessfulFilter = new IntentFilter(Constants.BR_PARTICIPANT_INSERT_UNSUCCESSFUL);
    	IntentFilter alreadyExistsfulFilter = new IntentFilter(Constants.BR_PARTICIPANT_INSERT_ALREADY_EXISTS);
    	mContext.registerReceiver(mBroadcastReceiver, successfulFilter);
    	mContext.registerReceiver(mBroadcastReceiver, unsuccessfulFilter);
    	mContext.registerReceiver(mBroadcastReceiver, alreadyExistsfulFilter);
    }
	
	@Override
    protected void onDestroy() {
    	this.unregisterReceiver(mBroadcastReceiver);
    	super.onDestroy();
    }
}
