package com.dporem.queues.parseclasses;

public class History {
	public final static String KEY_OBJCTIVEID = "objectId";
	public final static String KEY_CREATED_AT = "createdAt";	
	public final static String KEY_USEROID = "userOID";
	public final static String KEY_DELETED = "deleted";	
	public final static String KEY_QUEUEOID = "queueOID";
	public final static String KEY_ACTION_PERFORMED = "actionPerformed";
	
	public final static String ACTION_DELETED = "act_0";
	public final static String ACTION_JUMPED_IN_LINE = "act_2";
	public final static String ACTION_HIS_TURN_DONE = "act_1";	
	
	private String createdAt;
	private String queueOID;
	private String actionPerformed;
	
	
	public History(String createdAt, String queueOID, String actionPerformed) {		
		this.createdAt = createdAt;
		this.queueOID = queueOID;
		this.actionPerformed = actionPerformed;
	}

	public History() {
		
	}

	public String getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	public String getQueueOID() {
		return queueOID;
	}


	public void setQueueOID(String queueOID) {
		this.queueOID = queueOID;
	}


	public String getActionPerformed() {
		return actionPerformed;
	}


	public void setActionPerformed(String actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	
}
