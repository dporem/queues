package com.dporem.queues.parseclasses;

public class QPRelate {
	public final static String KEY_OBJCTIVEID = "objectId";
	public final static String KEY_QUEUE_OID = "queueOID";
	public final static String KEY_PARTICIPANT_OID = "participantOID";
	public final static String KEY_USEROID = "userOID";
	public final static String KEY_DELETED = "deleted";
	public final static String KEY_JUMPEDTOPAY = "jumpedToPay";
	public final static String KEY_TURN = "turn";
	
	private String objectId;
	private String queueOID;
	private String participantOID;
	private int turn;
	private int jumpedToPay;
	
	public QPRelate(String objectId, String queueOID, String participantOID, int turn, int jumpedToPay) {
		super();
		this.objectId = objectId;
		this.queueOID = queueOID;
		this.participantOID = participantOID;
		this.turn = turn;
		this.jumpedToPay = jumpedToPay;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getQueueOID() {
		return queueOID;
	}

	public void setQueueOID(String queueOID) {
		this.queueOID = queueOID;
	}

	public String getParticipantOID() {
		return participantOID;
	}

	public void setParticipantOID(String participantOID) {
		this.participantOID = participantOID;
	}
	
	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public int getJumpedToPay() {
		return jumpedToPay;
	}

	public void setJumpedToPay(int jumpedToPay) {
		this.jumpedToPay = jumpedToPay;
	}
}
