package com.dporem.queues.parseclasses;


public class Users{
	public final static String KEY_OBJECTID = "objectId";
	public final static String KEY_USERNAME = "username";
	public final static String KEY_PASSHASH = "passHash";
	public final static String KEY_EMAIL = "email";
	public final static String KEY_DELETED = "deleted";
	
	private static Users instance = null;
	
	public static Users getInstance() {
        if(instance == null) {
           instance = new Users();
        }
        return instance;
    }
	
	private Users(String objectId, String username, String passHash, String email) {		
		this.objectId = objectId;
		this.username = username;
		this.passHash = passHash;
		this.email = email;
	}
	
	private Users(){
		
	}	
			
	private String objectId;
	private String username;
	private String passHash;
	private String email;
	
	private boolean isUserLogged = false;
	
	public String getObjectID() {
		return objectId;
	}
	
	public void setObjectID(String objectId) {
		this.objectId = objectId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassHash() {
		return passHash;
	}
	
	public void setPassHash(String passHash) {
		this.passHash = passHash;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isUserLogged() {
		return isUserLogged;
	}

	public void setUserLogged(boolean isUserLogged) {
		this.isUserLogged = isUserLogged;
	}
}
