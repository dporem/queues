package com.dporem.queues.parseclasses;

public class Queue {
	public final static String KEY_OBJCTIVEID = "objectId";
	public final static String KEY_NAME = "name";
	public final static String KEY_USEROID = "userOID";
	public final static String KEY_DELETED = "deleted";
	public final static String KEY_TURN = "turn";
	
	private String objectId;
	private String name;
	private int turn; 
	
	public Queue(String objectId, String name, int turn) {
		super();
		this.objectId = objectId;
		this.name = name;
		this.turn = turn;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}
}
